# libI386
**LibI386** is a library for controlling X86 family processor semantics.
Currently it supports processors starting from 80386, 64-bit processor support
is not present.
