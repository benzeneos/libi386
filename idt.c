#include <i386/idt.h>

void
i386_idt_encode (const i386_idt *norm, i386_idt_raw *raw)
{
  /* Split fields which need it */
  raw->offset_low = norm->offset & 0xFFFF;
  raw->offset_high = (norm->offset >> 16) & 0xFFFF;
  
  /* Encode other fields */
  raw->selector = norm->selector;
  raw->zero = 0;
  raw->type = norm->type & 0xF;
  raw->seg = norm->segment ? 1 : 0;
  raw->dpl = norm->dpl & 0x3;
  raw->pr = norm->present ? 1 : 0;
}

void
i386_idt_decode (const i386_idt_raw *raw, i386_idt *norm)
{
  norm->offset = (((uint32_t) raw->offset_high) << 16) | raw->offset_low;
  norm->selector = raw->selector;
  norm->type = raw->type & 0xF;
  norm->segment = raw->seg ? true : false;
  norm->dpl = raw->dpl & 0x3;
  norm->present = raw->pr ? true : false;
}

void
i386_lidt (i386_idtr idtr)
{
  asm volatile ("lidt %0" : : "m" (idtr));
}

i386_idtr
i386_sidt (void)
{
  i386_idtr idtr;
  asm volatile ("sidt %0" : "=m" (idtr));
  return idtr;
}
