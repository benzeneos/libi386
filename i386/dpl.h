#ifndef LIBI386_DPL_H
#define LIBI386_DPL_H

typedef unsigned char i386_dpl;

#define I386_DPL_MIN 0
#define I386_DPL_MAX 3

#define I386_DPL_KERNEL 0
#define I386_DPL_USER 3

#endif
