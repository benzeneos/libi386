#ifndef LIBI386_CDEFS_H
#define LIBI386_CDEFS_H

#if defined (__has_attribute__)
# define LIBI386_HAS_ATTRIBUTE(x) __has_attribute__ (x)
#elif defined (__has_attribute)
# define LIBI386_HAS_ATTRIBUTE(x) __has_attribute (x)
#elif defined (has_attribute)
# define LIBI386_HAS_ATTRIBUTE(x) has_attribute (x)
#endif

#if defined (__cplusplus)
# define LIBI386_BEGIN_DECLS extern "C" {
# define LIBI386_END_DECLS }
#else
# define LIBI386_BEGIN_DECLS
# define LIBI386_END_DECLS
#endif

#if defined (__GNUC__) || LIBI386_HAS_ATTRIBUTE (__packed__)
# define LIBI386_PACKED __attribute__ ((__packed__))
#elif LIBI386_HAS_ATTRIBUTE (packed)
# define LIBI386_PACKED __attribute__ ((packed))
#else
# define LIBI386_PACKED
# warn Please use a compiler with structure packing
#endif

#endif /* LIBI386_CDEFS_H */
