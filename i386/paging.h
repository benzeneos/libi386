#ifndef LIBI386_PAGING_H
#define LIBI386_PAGING_H

#include <i386/cdefs.h>

typedef struct
{
  unsigned int page_addr : 20;
  unsigned int avail : 3;
  unsigned int ignored : 1;
  unsigned int zero : 1;
  unsigned int dirty : 1;
  unsigned int accessed : 1;
  unsigned int cache_dis : 1;
  unsigned int write_through : 1;
  unsigned int user : 1;
  unsigned int rw : 1;
  unsigned int present : 1;
} LIBI386_PACKED i386_pagetable;

typedef struct
{
  unsigned int table_addr : 20;
  unsigned int avail : 3;
  unsigned int ignored : 1;
  unsigned int size : 1;
  unsigned int zero : 1;
  unsigned int accessed : 1;
  unsigned int cache_dis : 1;
  unsigned int write_through : 1;
  unsigned int user : 1;
  unsigned int rw : 1;
  unsigned int present : 1;
} LIBI386_PACKED i386_pagedir;

LIBI386_BEGIN_DECLS

void i386_set_pagedir (i386_pagedir *);
void i386_enable_paging (void);
void i386_disable_paging (void);

LIBI386_END_DECLS

#endif /* LIBI386_PAGING_H */
