#ifndef LIBI386_GDT_H
#define LIBI386_GDT_H

#include <i386/cdefs.h>
#include <i386/dpl.h>

#include <stdbool.h>
#include <stdint.h>

typedef struct i386_gdt
{
  uint32_t base;
  uint32_t limit;
  bool ac;
  bool rw;
  bool dc;
  bool ex;
  bool seg;
  i386_dpl dpl;
  bool pr;
  bool av;
  bool long_mode;
  bool sz;
} i386_gdt;

typedef struct i386_gdt_raw
{
  unsigned int limit_low : 16;
  unsigned int base_low : 24;
  unsigned int ac : 1;
  unsigned int rw : 1;
  unsigned int dc : 1;
  unsigned int ex : 1;
  unsigned int seg : 1;
  unsigned int dpl : 2;
  unsigned int pr : 1;
  unsigned int limit_high : 4;
  unsigned int av : 1;
  unsigned int lm : 1;
  unsigned int sz : 1;
  unsigned int gr : 1;
  unsigned int base_high : 8;
} LIBI386_PACKED i386_gdt_raw;

typedef struct i386_gdtr
{
  uint16_t size;
  uint32_t base;
} LIBI386_PACKED i386_gdtr;

#define I386_GDT_FLAG_PR 0x80
#define I386_GDT_FLAG_SEG 0x10
#define I386_GDT_FLAG_EX 0x08
#define I386_GDT_FLAG_DC 0x04
#define I386_GDT_FLAG_RW 0x02
#define I386_GDT_FLAG_AC 0x1

#define I386_GDT_ACC_GR 0x08
#define I386_GDT_ACC_SZ 0x04

#define I386_GDT_MAX 65536

LIBI386_BEGIN_DECLS

void i386_gdt_encode (const i386_gdt *, i386_gdt_raw *);
void i386_gdt_decode (const i386_gdt_raw *, i386_gdt *);

void i386_lgdt (i386_gdtr);
i386_gdtr i386_sgdt (void);

LIBI386_END_DECLS

#endif /* LIBI386_GDT_H */
