#ifndef LIBI386_IDT_H
#define LIBI386_IDT_H

#include <i386/cdefs.h>
#include <i386/dpl.h>

#include <stdbool.h>
#include <stdint.h>

typedef unsigned char i386_idt_type;

typedef struct i386_idt
{
  uint32_t offset;
  uint16_t selector;
  i386_idt_type type;
  bool segment;
  i386_dpl dpl;
  bool present;
} i386_idt;

typedef struct i386_idt_raw
{
  unsigned int offset_low : 16;
  unsigned int selector : 16;
  unsigned int zero : 8;
  unsigned int type : 4;
  unsigned int seg : 1;
  unsigned int dpl : 2;
  unsigned int pr : 1;
  unsigned int offset_high : 16;

} LIBI386_PACKED i386_idt_raw;

typedef struct i386_idtr
{
  uint16_t size;
  uint32_t addr;
} LIBI386_PACKED i386_idtr;

#define I386_IDT_MAX 256

#define I386_IDT_TYPE_TASK_386 0x5
#define I386_IDT_TYPE_INT_286 0x6
#define I386_IDT_TYPE_TRAP_286 0x7
#define I386_IDT_TYPE_INT_386 0xE
#define I386_IDT_TYPE_TRAP_386 0xF

LIBI386_BEGIN_DECLS

void i386_idt_encode (const i386_idt *, i386_idt_raw *);
void i386_idt_decode (const i386_idt_raw *, i386_idt *);

void i386_lidt (i386_idtr);
i386_idtr i386_sidt (void);

LIBI386_END_DECLS

#endif
