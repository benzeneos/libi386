#include <i386/paging.h>

#include <stdint.h>

void
i386_set_pagedir (i386_pagedir *dir)
{
  __asm__ volatile ("mov %0, %%cr3" : : "a" (dir));
}

void
i386_enable_paging (void)
{
  uint32_t cr0 = 0;
  __asm__ volatile ("mov %%cr0, %0\n" : "=a" (cr0));
  cr0 |= 0x80000000;
  __asm__ volatile ("mov %0, %%cr0\n" : : "a" (cr0));
}

void
i386_disable_paging (void)
{
  uint32_t cr0 = 0;
  __asm__ volatile ("mov %%cr0, %0\n" : "=a" (cr0));
  cr0 ^= 0x80000000;
  __asm__ volatile ("mov %0, %%cr0\n" : : "a" (cr0));
}
