#include <i386/gdt.h>

#include <stdint.h>

_Static_assert (sizeof (struct i386_gdt_raw) == 8, "Packing not working");

void
i386_gdt_encode (const i386_gdt *norm, i386_gdt_raw *raw)
{
  /* Check whether we need to adjust granularity */
  uint32_t limit = norm->limit;
  if (limit > 0xFFFFF)
    {
      raw->gr = 1;
      limit /= 0x1000;
    }
  else
    {
      raw->gr = 0;
    }

  /* Encode the split values... */
  raw->base_low = norm->base & 0xFFFFFF;
  raw->base_high = (norm->base & 0xFF000000) >> 24;
  raw->limit_low = norm->limit & 0xFFFF;
  raw->limit_high = (norm->limit & 0xF0000) >> 16;

  /* ...and copy all the values which just have to be copied */
  raw->ac = norm->ac;
  raw->rw = norm->rw;
  raw->dc = norm->dc;
  raw->ex = norm->ex;
  raw->seg = norm->seg;
  raw->dpl = norm->dpl & 0x3;
  raw->pr = norm->pr;
  raw->av = norm->av;
  raw->lm = norm->long_mode;
  raw->sz = norm->sz;
}

void
i386_gdt_decode (const i386_gdt_raw *raw, i386_gdt *norm)
{
  /* Unsplit the split values... */
  norm->base = (((uint32_t) raw->base_high) << 24) | raw->base_low;
  norm->limit = (((uint32_t) raw->limit_high) << 16) | raw->limit_low;

  if (raw->gr)
    norm->limit *= 0x1000;

  /* And copy the flags */
  norm->ac = raw->ac;
  norm->rw = raw->rw;
  norm->dc = raw->dc;
  norm->ex = raw->ex;
  norm->seg = raw->seg;
  norm->dpl = raw->dpl & 0x3;
  norm->pr = raw->pr;
  norm->av = raw->av;
  norm->long_mode = raw->lm;
  norm->sz = raw->sz;
}

void
i386_lgdt (i386_gdtr gdtr)
{
  asm volatile ("lgdt %0" : : "m" (gdtr));
}

i386_gdtr
i386_sgdt (void)
{
  i386_gdtr gdtr;
  asm volatile ("sgdt %0" : "=m" (gdtr));
  return gdtr;
}
